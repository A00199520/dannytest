
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class HelloWorldTest {

	WebDriver driver;

	@Before
	public void init() {
	}

	/*
	 * @Test public void testHelloWorld() { HelloWorld hw = new HelloWorld();
	 * assertEquals("hello world", hw.helloworld()); }
	 */

	@Test
	public void testHelloWorldSelenium() throws InterruptedException {
		ChromeOptions coptions = new ChromeOptions();
		//coptions.addArguments("--start-maximized", "--headless", "--no-sandbox");
		//WebDriverManager.chromedriver().version("79.0.3945.36").setup();
		WebDriverManager.chromedriver().setup();

		driver = new ChromeDriver(coptions);
		driver.get("http://wikipedia.org");
		Thread.sleep(2000);
		// hi guys
		driver.findElement(By.xpath("/html/body/div[2]/form/fieldset/div/input")).sendKeys("Interstellar (film)");
		driver.findElement(By.xpath("/html/body/div[2]/form/fieldset/button")).click();

		String actualTitle = driver.getTitle();
		String expectedTitle = "Interstellar (film) - Wikipedia";
		assertEquals(expectedTitle, actualTitle);

		driver.close();
	}


//	@Test
//	public void testThingy() {
//		System.setProperty("webdriver.chrome.driver", "C:\\Users\\a00268673\\.m2\\repository\\webdriver\\chromedriver\\win32\\79.0.3945.36\\chromedriver.exe");
//		WebDriver driver = new ChromeDriver();
//		driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
//		//driver.findElement(By.linkText("Create Account")).click();
//		driver.findElement(By.xpath("html/body/form/div[3]/div[2]/div/div[2]/a")).click();
//		// fill out form
//		driver.findElement(By.name("ctl00$MainContent$txtFirstName")).sendKeys("Mary Smith");
//		driver.findElement(By.id("MainContent_txtEmail")).sendKeys("ms@test.com");
//		driver.findElement(By.xpath("//*[@id='MainContent_txtHomePhone']")).sendKeys("123456");
//		driver.findElement(By.cssSelector("input[id='MainContent_txtPassword']")).sendKeys("mspass");
//		driver.findElement(By.cssSelector("input[id='MainContent_txtVerifyPassword']")).sendKeys("mspass");
//		driver.findElement(By.id("MainContent_Female")).click();
//		new Select(driver.findElement(By.id("MainContent_menuCountry"))).selectByVisibleText("Germany");
//		driver.findElement(By.name("ctl00$MainContent$checkWeeklyEmail")).click();
//		//driver.findElement(By.id("MainContent_btnSubmit"));
//		String confirmation = driver.findElement(By.id("MainContent_lblTransactionResult")).getText();
//		System.out.println("Conf " + confirmation);
//	}

}
